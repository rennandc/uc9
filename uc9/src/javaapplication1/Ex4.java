
package javaapplication1;

import java.util.Scanner;


public class Ex4 {
    public static void main(String[] args) {
        double carros,salario,valorVendas,comissaoFixa,valorReciboCadaCarro,salarioFinal;
        Scanner valor = new Scanner(System.in);
        System.out.print("Digite a quantidade de carros vendidos:");
        carros = valor.nextDouble();
        System.out.print("Digite o seu salário fixo:");
        salario = valor.nextDouble();
        System.out.print("Digite a sua comissão fixa:");
        comissaoFixa = valor.nextDouble();
        System.out.print("Digite o valor total de suas vendas:");
        valorVendas = valor.nextDouble();
        System.out.print("Digite o valor recebido por cada carro:");
        valorReciboCadaCarro = valor.nextDouble();
        salarioFinal = salario + (carros * comissaoFixa) + (valorVendas * 0.05) + (carros + valorReciboCadaCarro);
        System.out.println(" O seu salário final é : " + salarioFinal);
        
       
    }
    
}
