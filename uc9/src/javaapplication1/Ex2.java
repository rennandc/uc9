
package javaapplication1;

import java.util.Scanner;


public class Ex2 {
    public static void main(String[] args) {
        float salario; 
        float ajuste;
        float salarioFinal;
        Scanner valor = new Scanner(System.in);
        System.out.print("Digite o seu salário atual:");
        salario = valor.nextFloat();
        System.out.print("Digite o seu ajuste:");
        ajuste = valor.nextFloat();
        salarioFinal = salario + ajuste;
        System.out.println("O seu novo salário é : " + salarioFinal);
        
    }
    
}
